import React from 'react';
// This module was created to provide the necessary information about the user/client who is using the app.

// 1. Create the 'context' about the subject.
	// createContext() will allow us to create a context 'objec' that will allow us store information about the subject.
const UserContext = React.createContext(); //container for the information

// 2. Acquire for the provider property of the newly created context object.
	// visualize:
	// UserContext = {
	// 	propsAboutTheUser: value
		// Provider:
	// }
	// Provider => this will allow us to use its utility to permit subcomponents of our app to consume/use the available information about our subjext.
export const UserProvider = UserContext.Provider;

// 3. Identify the module on your app that will take the role of the provider.

// Expose the context object for the consumers
export default UserContext;