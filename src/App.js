import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar';


// Acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import Catalog from './pages/Courses';
import ErrorPage from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout'
import CourseView from './pages/CourseView';
import AddCourse from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse';

// Acquire the provider utility in this entry point
import { UserProvider } from './UserContext';

// implement page routing in our app
// acquire the utilities from react-router-dom
import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom'
// BrowserRouter -> this is a standard library component for routing in react. This enables navigation amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
// as -> alias keyword in JS.

// Routes => it's a new component introduced in V6 of react-router-dom which task is to allow switching between locations.

// JSX Component -> self closing tag
// syntax: <Element />

import './App.css';

function App() {

  // The role of the Provider was assigned to the Apss.js which means all the information that we will declare here will automatically become a 'global' scope.
  // initialize a state of the user to provide/identify the status of the client who is using the app.

  // id => to reference the user.
  //isAdmin => role of the user. 
  const [user, setUSer] = useState({
    id: null,
    isAdmin: null
  }) //to bind the user to the default state.

  // Feed the information stated here to the consumers.

  // Create a function that will unset/unmount the user
  const unsetUser = () => {
    // Clear out the saved data in the local storage.
    // Remove all data in the local storage.
    localStorage.clear();
    // Revert the state of the user back to null.
    setUSer({
      id: null,
      isAdmin: null
    });
  }

  // Create a side effect that will send a request to API collection to get the identity of the user using the access token which is saved in the browser.

  useEffect(() => {
    // identify the state of the user
    // console.log(user);
    // Mount or campaign the user to the app so that he will be recognized, using the token.

    // retrieve the token from the browser storage.
    let token =  localStorage.getItem('accessToken');
    // console.log(token);

    // In this request we will pass down a token, hence, the 'options' is required. 
    fetch('https://glacial-everglades-19835.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json()).then(convertedData => {
      // console.log(convertedData)
      // Identigy the procedures that will happen if the info abot the user is retrieved successfully.
      // Change the current state of the user.
      if (typeof convertedData._id !== "undefined") {
          setUSer({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
          // console.log(user);
      } else {
          // If the condition above is not met, keep the values in null
          setUSer({
            id: null,
            isAdmin: null
          });          
      }
    });

  },[user]);

  return (    
    <UserProvider value={{user, setUSer, unsetUser}}>
      <Router>
        <AppNavBar />
        <Routes>  
          <Route path='/' element={<Home />}  />         
          <Route path='/register' element={<Register />}  />
          <Route path='/courses' element={<Catalog />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/courses/view/:id' element={<CourseView />} />
          <Route path='/add-course' element={<AddCourse />} />
          <Route path='/update-course' element={<UpdateCourse />} />
          <Route path='*' element={<ErrorPage />} />
        </Routes>        
      </Router>      
    </UserProvider>
  );
}

export default App;

