// Identify the components that we will use to build the page
import Header from './../components/Banner';

// Pass down 'Props'

const data = {
	title: 'Page Not Found',
	content: 'The page you are looking for Does not Exist'
}

export default function ErrorPage(){
	return(
		<Header bannerData={data} />
	);
};