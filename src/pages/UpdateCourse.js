import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data ={
	title: 'Update Courses Here',
	content: 'Input information on fields to update the courses'
}

export default function UpdateCourse() {
	

	const courseUpdate = (update) => {
		update.preventDefault();
		return(
			Swal.fire({
				icon: "success",
				title: "Update is Successful",
				text: "Course has been updated"
			})
		);

	};	
	
	return(
		<>
			<Hero bannerData={data} />

			<Container>
				<h1 className="text-center">Update Course Form</h1>
				<Form onSubmit={e => courseUpdate(e)}>
					<Form.Group>
						<Form.Label>Course Name</Form.Label>
						<Form.Control 
						type="text"
						placeholder="Enter Course Name"
						required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description</Form.Label>
						<Form.Control 
						type="text"
						placeholder="Enter Course Description"
						required
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price</Form.Label>
						<Form.Control 
						type="number"
						placeholder="Ex. 500.00"
						required
						/>
					</Form.Group>
					{['checkbox'].map((type) => (
					    <div key={`default-${type}`} className="mb-3">
					      <Form.Check 
					        type={type}
					        id={`default-${type}`}
					        label={`Check if this course is Active`}
					      />					      
					    </div>
					  ))}
					<Button variant="success" className="btn-block" type="submit">Update Course</Button>
				</Form>
			</Container>
		</>
	);
};