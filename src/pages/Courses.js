// Identify which components will be displayed
import {useState, useEffect} from 'react';
import Hero from './../components/Banner';
import CourseCard from './../components/CourseCard';
import {Container, Row, Col} from 'react-bootstrap';

const bannerDetails = {
	title: 'Course Catalog',
	content: 'Browse through our Catalog of Courses'
}

// Catalog all Active Courses
	// 1. Container for each course that we will retrieve from the database
	// 2. Declare state for the courses collection. By default it is an empty array.
	// 3. Create a side effect that will send a request to our backend api project.
	// 4. Pass down the retrieved resources inside the component as props.

export default function Courses() {

	// this array/storage will be used to save our course components for all active courses
	const [coursesCollection, setCourseCollection] = useState([]);

	// create an 'effect' using our Effect Hook, provide a list of dependencies
	// the effect that we're goind to create is fetching data from the server.
	useEffect(() => {
		// fetch() => is a JS method which allows us to pass/create a request to an API.
		// Syntax: fetch(<request URL>, {OPTIONS})
		// GET HTTP METHOD especially since we do not need to insert a req body or apps a token => NO NEED TO INSERT OPTIONS

		// Remember: once you send a request to an endpoint, a promise is returned as a result.
		// 3 states of promise: (fulfilled, reject, pending)
		// We need to be able to handle the outcome of the promise.
		// We need to make the respone usable on the frontend side. We need to convert it to a JSON format
		// Upon converting the fetched data to a json format a new promise will be executed.
		fetch('https://glacial-everglades-19835.herokuapp.com/courses/').then(res => res.json()).then(convertedData => {/*console.log(convertedData)*/
			// we want the resources to be displayed in the page
			// since the data is stored in an array storage, we will iterate the array to explicitly get each documents one by one.
			// there will be multiple course card component that will be rendered that corresponds to each document retrieved from the database. We need to utilizea storage to contain the cards that will be rendered
			// key attribute => as a reference to avoid rendering duplicates of the same element/object.
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			})) 
		});
	},[]);

	return(
		<>
			<Hero bannerData={bannerDetails} />
			<Container>
				<Row>					
					{coursesCollection}
				</Row>
			</Container>
		</>
	);
};